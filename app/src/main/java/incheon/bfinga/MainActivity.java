package incheon.bfinga;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import cz.msebera.android.httpclient.Header;
import incheon.bfinga.AttendanceLists.AttendanceList;
import incheon.bfinga.utils.RestClient;

public class MainActivity extends AppCompatActivity implements BeaconConsumer {
    private DoAttendance doAttendance;
    private AttendanceList attendanceList;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private RestClient restClient;
    private String userid;
    private TextView headtxt;
    private Button dattlist, dmain, dsetting;
    private DrawerLayout drawer;
    private Toolbar toolbar;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    private View header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { //퍼미션 요청
            requestPermissions(new String[]{Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.USE_FINGERPRINT, Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE
                    ,Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        restClient = new RestClient(this);//restful
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

        drawer = (DrawerLayout) findViewById(R.id.main_drawer_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        header = navigationView.inflateHeaderView(R.layout.nav_header_nevigation);//네비게이션, 툴바 설정

        headtxt = (TextView) header.findViewById(R.id.headtxt);

        AppStatus.setbm(BeaconManager.getInstanceForApplication(this));
        AppStatus.setview((LinearLayout) findViewById(R.id.layoutview));
        AppStatus.settooltxt((TextView) findViewById(R.id.toolbartxt));//singleton에 객체들 설정

        // ibeacon parser 추가
        AppStatus.getbm().getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));
        AppStatus.getbm().bind(this);

        doAttendance = new DoAttendance(this);//출석체크 객체 생성
        attendanceList = new AttendanceList(this);

        dattlist = (Button) findViewById(R.id.dattlist);
        dattlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attendanceList.init();
                drawer.closeDrawers();
            }
        });
        dmain = (Button) findViewById(R.id.dmain);
        dmain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doAttendance.init();
                drawer.closeDrawers();
            }
        });
        dsetting = (Button) findViewById(R.id.dsetting);
        dsetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attendanceList.init();
                drawer.closeDrawers();
            }
        });

        init();
    }


    @Override
    public void onResume() {
        super.onResume();
        AppStatus.getbm().bind(this);
    }

    @Override
    public void onRestart(){
        super.onRestart();
        if(preferences.getBoolean("submit",false)) {
            subinit();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        AppStatus.getbm().unbind(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void onBeaconServiceConnect() {}


    private void init(){ //사용자 인증
        if (!preferences.getBoolean("submit",false)) {
            View dialogView = getLayoutInflater().inflate(R.layout.dialog_user, null);
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setView(dialogView);
            final AlertDialog userdialog = builder.create();
            userdialog.setCanceledOnTouchOutside(false);
            userdialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    if (preferences.getString("user", "") == "") {
                        Toast.makeText(MainActivity.this, "반드시 등록하셔야 합니다", Toast.LENGTH_SHORT).show();
                        userdialog.show();
                    }
                }
            });
            userdialog.show();
            final EditText useredit = (EditText) userdialog.findViewById(R.id.useredit);
            Button userbtn = (Button) userdialog.findViewById(R.id.userbtn);
            userbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (useredit.getText().toString().equals("")) {
                        Toast.makeText(MainActivity.this, "공백은 쓸 수 없습니다", Toast.LENGTH_SHORT).show();
                    } else {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("등록");
                        builder.setMessage("입력하신 " + useredit.getText() + " 가 확실합니까?");
                        builder.setCancelable(false);
                        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                final ProgressDialog dialog = ProgressDialog.show(MainActivity.this, "", "등록중...", true);
                                dialog.setCancelable(true);
                                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                    @Override
                                    public void onCancel(DialogInterface dialogInterface) {
                                        Toast.makeText(MainActivity.this, "등록취소합니다", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                restClient.get("getuserclass/" + useredit.getText().toString(), null, new JsonHttpResponseHandler() {
                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                                        dialog.dismiss();
                                        Toast.makeText(MainActivity.this, "등록 실패하였습니다 학번을 확인해주세요", Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                                        Toast.makeText(MainActivity.this, response.toString(), Toast.LENGTH_SHORT).show();
                                        if (response.toString().equals("[]")) {
                                            dialog.dismiss();
                                            Toast.makeText(MainActivity.this, "등록 실패하였습니다 학번을 확인해주세요", Toast.LENGTH_SHORT).show();
                                        } else {
                                            AppStatus.setclalist(response.length(),"submit");
                                            editor.putInt("classcount", response.length());
                                            for (int i = 0; i < response.length(); i++) {
                                                try {
                                                    JSONObject jsonObject = response.getJSONObject(i);
                                                    AppStatus.setclalist(i,jsonObject.getString("classid"));
                                                    editor.putString("class" + i, jsonObject.getString("classid"));
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                            dialog.dismiss();
                                            userdialog.dismiss();
                                            AppStatus.setuserid(useredit.getText().toString());
                                            userid = AppStatus.getuserid();
                                            editor.putString("user", userid);
                                            editor.commit();
                                            subinit();
                                        }
                                    }
                                });
                            }
                        }).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        });
                        builder.show();
                    }
                }
            });
        } else {
            AppStatus.setuserid(preferences.getString("user", ""));
            userid = AppStatus.getuserid();
            subinit();
        }
    }

    private void subinit(){
        if(!preferences.getBoolean("submit",false)){
            editor.putString("AUID",UUID.randomUUID().toString());
            editor.commit();
            RequestParams params = new RequestParams();
            params.put("AUID",preferences.getString("AUID",""));
            params.put("studentid",userid);
            restClient.post("setclientid", params, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if(responseString.equals("dfail")){
                        Toast.makeText(MainActivity.this,"기존앱을 삭제 후 재설치 혹은 어플리케이션 데이터를 지우셨을 경우 당일엔 어플을 이용하실수 없습니다.",Toast.LENGTH_LONG).show();
                        finish();
                    }else if(responseString.equals("fail")){
                        Toast.makeText(MainActivity.this,"어플리케이션 등록에 실패하였습니다 재시도 해주세요",Toast.LENGTH_LONG).show();
                        finish();
                    }else if(responseString.equals("suc")){
                        Toast.makeText(MainActivity.this,"어플리케이션 등록에 성공하였습니다." + userid + " 로 등록되었습니다",Toast.LENGTH_LONG).show();
                        doAttendance.init();
                        headtxt.setText(userid);
                        editor.putBoolean("submit",true);
                        editor.commit();
                    }else if(responseString.equals("alr")){
                        Toast.makeText(MainActivity.this,"이미 등록되어있어 재등록하였습니다.",Toast.LENGTH_LONG).show();
                        doAttendance.init();
                        headtxt.setText(userid);
                        editor.putBoolean("submit",true);
                        editor.commit();
                    }
                }
            });
        } else {
            RequestParams params = new RequestParams();
            params.put("AUID",preferences.getString("AUID",""));
            params.put("studentid",userid);
            restClient.post("checkclientid", params, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if(responseString.equals("dfail")){
                        Toast.makeText(MainActivity.this,"기존앱을 삭제 혹은 어플리케이션 데이터를 지우신 후 재설치 하셨을 경우 당일엔 어플을 이용하실수 없습니다.",Toast.LENGTH_LONG).show();
                        finish();
                    }else if(responseString.equals("fail")){
                        Toast.makeText(MainActivity.this,"어플리케이션 인증에 실패하였습니다 다시 시도해주세요.",Toast.LENGTH_LONG).show();
                        finish();
                    }else if(responseString.equals("no")){
                        Toast.makeText(MainActivity.this,"등록되어있지 않은 사용자입니다.",Toast.LENGTH_LONG).show();
                        finish();
                    }else if(responseString.equals("suc")){
                        Toast.makeText(MainActivity.this,userid + "님 환영합니다.",Toast.LENGTH_SHORT).show();
                        AppStatus.setclalist(preferences.getInt("classcount",0),"submit");
                        doAttendance.init();
                        for (int i = 0; i < preferences.getInt("classcount", 0); i++) {
                            AppStatus.setclalist(i,preferences.getString("class" + i, ""));
                        }
                        headtxt.setText(userid);
                    }
                }
            });
        }
    }
}
