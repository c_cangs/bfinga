package incheon.bfinga;


import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.altbeacon.beacon.BeaconManager;

/**
 * Created by kyun on 2016-10-26.
 */
public class AppStatus {
    private static BeaconManager beaconManager = null;
    private static LinearLayout layoutview = null;
    private static TextView toolbartxt = null, statustxt = null;
    private static String userid = null;
    private static Button attnbtn = null;
    private static String[] classlist = null;

    public static  void setbm (BeaconManager bm){
        beaconManager = bm;
    }
    public static BeaconManager getbm(){
        return beaconManager;
    }

    public static void setview(LinearLayout v){
        layoutview = v;
    }
    public static void settingview(View v){
        layoutview.removeAllViews();
        layoutview.addView(v);
    }

    public static void settooltxt(TextView textView){
        toolbartxt = textView;
    }
    public static void settingtooltxt(String s){
        toolbartxt.setText(s);
    }

    public static void setstatustxt(TextView textView){
        statustxt = textView;
    }
    public static void settingstatustxt(String s){
        statustxt.setText(s);
    }

    public static void setuserid(String s){
        userid = s;
    }
    public static String getuserid(){
        return userid;
    }

    public static void setattnbtn(Button button){
        attnbtn = button;
    }
    public static Button getattnbtn(){
        return attnbtn;
    }

    public static void setclalist(int i,String s){
        if(s == "submit") classlist = new String[i];
        else classlist[i] = s;
    }
    public static String[] getclalist(){
        return classlist;
    }

}
