package incheon.bfinga.utils;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.samsung.android.sdk.SsdkUnsupportedException;
import com.samsung.android.sdk.pass.Spass;
import com.samsung.android.sdk.pass.SpassFingerprint;
import com.samsung.android.sdk.pass.SpassInvalidStateException;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import incheon.bfinga.AppStatus;
import incheon.bfinga.MainActivity;
import incheon.bfinga.R;

/**
 * Created by kyun on 2016-08-31.
 */
public class samsungfinger {

    private SpassFingerprint mSpassFingerprint;
    private Spass mSpass;
    private Context mContext;
    private ArrayList<Integer> designatedFingers = null;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private boolean setting;
    private int mainfinger;
    private RestClient restClient;
    private String uuid, userid, fingername;
    private View dialogView;
    private AlertDialog.Builder builder;
    private AlertDialog userdialog;
    private TextView ditxt;
    private ImageView fingerimg;
    private Handler mHandler;

    private boolean onReadyIdentify = false;
    private boolean onReadyEnroll = false;
    private boolean onDoingIdentify = false;
    private boolean onNomissIdentify = false;
    private boolean hasRegisteredFinger = false;
    private boolean willregistfinger = true;
    private boolean needRetryIdentify = false;

    private boolean isFeatureEnabled_fingerprint = false;
    private boolean isFeatureEnabled_index = false;
    private boolean isFeatureEnabled_uniqueId = false;
    private boolean isFeatureEnabled_custom = false;
    private boolean isFeatureEnabled_backupPw = false;


    public samsungfinger(Context context, String uuid) {
        this.mContext = context;
        this.uuid = uuid;
        this.userid = AppStatus.getuserid();
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = preferences.edit();
        restClient = new RestClient(context);
        dialogView = ((MainActivity)context).getLayoutInflater().inflate(R.layout.dialog_fingerprient, null);
        ditxt = (TextView) dialogView.findViewById(R.id.ditxt);
        fingerimg = (ImageView) dialogView.findViewById(R.id.fingerimg);
        mHandler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                startIdentify();
            }
        };

        builder = new AlertDialog.Builder(context);
        builder.setView(dialogView);
        userdialog = builder.create();
        userdialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if(onDoingIdentify) {
                    cancelIdentify();
                    onDoingIdentify = false;
                }
            }
        });

        setting = preferences.getBoolean("setting",false);

        mSpass = new Spass();

        try {
            mSpass.initialize(context);
        } catch (SsdkUnsupportedException e) {
            Log.e("e","Exception: " + e);
        } catch (UnsupportedOperationException e) {
            Log.e("e","Fingerprint Service is not supported in the device");
            willregistfinger = false;
        }
        isFeatureEnabled_fingerprint = mSpass.isFeatureEnabled(Spass.DEVICE_FINGERPRINT);

        if (isFeatureEnabled_fingerprint) {
            mSpassFingerprint = new SpassFingerprint(context);
            if(setting){
                getFingerprintName(preferences.getInt("mainfinger",-1));
            }
            Log.i("i","Fingerprint Service is supported in the device.");
            Log.i("i","SDK version : " + mSpass.getVersionName());
        } else {
            Log.i("i","Fingerprint Service is not supported in the device.");
            toast("사용할수 없는 디바이스 입니다");
            willregistfinger = false;
            return;
        }

        try {
            hasRegisteredFinger = mSpassFingerprint.hasRegisteredFinger();
        } catch (UnsupportedOperationException e) {
            willregistfinger = false;
            Log.e("e","Fingerprint Service is not supported in the device");
            toast("사용할수 없는 디바이스 입니다");
        }

        isFeatureEnabled_index = mSpass.isFeatureEnabled(Spass.DEVICE_FINGERPRINT_FINGER_INDEX);
        isFeatureEnabled_custom = mSpass.isFeatureEnabled(Spass.DEVICE_FINGERPRINT_CUSTOMIZED_DIALOG);
        isFeatureEnabled_uniqueId = mSpass.isFeatureEnabled(Spass.DEVICE_FINGERPRINT_UNIQUE_ID);
        isFeatureEnabled_backupPw = mSpass.isFeatureEnabled(Spass.DEVICE_FINGERPRINT_AVAILABLE_PASSWORD);

        registerBroadcastReceiver();

    }

    public void init() {
        try { //등록된 지문 검사
            hasRegisteredFinger = mSpassFingerprint.hasRegisteredFinger();
        } catch (UnsupportedOperationException e) {
            willregistfinger = false;
            Log.e("e","Fingerprint Service is not supported in the device");
            toast("사용할수 없는 디바이스 입니다");
        }
        if (willregistfinger) {
            if (!hasRegisteredFinger) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage("먼저 기기에 지문을 등록하세요").setCancelable(true)
                        .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                registerFingerprint();
                            }
                        }).setNegativeButton("no", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.i("i","Please register finger first");
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.setTitle("");
                alertDialog.show();
            } else {
                if (setting) {
                    mainfinger = preferences.getInt("mainfinger", 0);
                    makeIdentifyIndex(mainfinger);
                    startIdentify();
                    userdialog.show();
                } else {
                    userdialog.show();
                    startIdentify();
                }
            }
        }
    }

    private BroadcastReceiver mPassReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (SpassFingerprint.ACTION_FINGERPRINT_RESET.equals(action)) {
                Toast.makeText(mContext, "all fingerprints are removed", Toast.LENGTH_SHORT).show();
            } else if (SpassFingerprint.ACTION_FINGERPRINT_REMOVED.equals(action)) {
                int fingerIndex = intent.getIntExtra("fingerIndex", 0);
                Toast.makeText(mContext, fingerIndex + " fingerprints is removed", Toast.LENGTH_SHORT).show();
            } else if (SpassFingerprint.ACTION_FINGERPRINT_ADDED.equals(action)) {
                int fingerIndex = intent.getIntExtra("fingerIndex", 0);
                Toast.makeText(mContext, fingerIndex + " fingerprints is added", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private SpassFingerprint.RegisterListener mRegisterListener = new SpassFingerprint.RegisterListener() {
        @Override
        public void onFinished() {
            onReadyEnroll = false;
            Log.i("i","RegisterListener.onFinished()");
        }
    };

    private void registerFingerprint() {
        if (onReadyIdentify == false) {
            if (onReadyEnroll == false) {
                onReadyEnroll = true;
                if (mSpassFingerprint != null) {
                    mSpassFingerprint.registerFinger(mContext, mRegisterListener);
                }
                Log.i("i","Jump to the Enroll screen");
            } else {
                Log.i("i","Please wait and try to register again");
            }
        } else {
            Log.i("i","Please cancel Identify first");
        }
    }

    private void registerBroadcastReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(SpassFingerprint.ACTION_FINGERPRINT_RESET);
        filter.addAction(SpassFingerprint.ACTION_FINGERPRINT_REMOVED);
        filter.addAction(SpassFingerprint.ACTION_FINGERPRINT_ADDED);
        mContext.registerReceiver(mPassReceiver, filter);
    };


    private void makeIdentifyIndex(int i) {
        if (designatedFingers == null) {
            designatedFingers = new ArrayList<Integer>();
        }
        for(int j = 0; j< designatedFingers.size(); j++){
            if(i == designatedFingers.get(j)){
                return;
            }
        }
        designatedFingers.add(i);
    }

    private void startIdentify() {
        onDoingIdentify = true;
        if (onReadyIdentify == false) {
            try {
                onReadyIdentify = true;
                if (mSpassFingerprint != null) {
                    setIdentifyIndex();
                    mSpassFingerprint.startIdentify(mIdentifyListener);
                }
                if (designatedFingers != null) {
                    Log.i("i","Please identify finger to verify you with " + designatedFingers.toString() + " finger");
                    if(setting) {
                        setditxt("등록하신 지문 " + fingername + " 으로 인식하세요");
                    }
                } else {
                    Log.i("i","Please identify finger to verify you");
                }
            } catch (SpassInvalidStateException ise) {
                onReadyIdentify = false;
                resetIdentifyIndex();
                if (ise.getType() == SpassInvalidStateException.STATUS_OPERATION_DENIED) {
                    Log.i("i","Exception: " + ise.getMessage());
                    setditxt("오류가 발생하였습니다.\n 잠시후 다시 시도해주세요.");
                }
            } catch (IllegalStateException e) {
                onReadyIdentify = false;
                resetIdentifyIndex();
                Log.i("i","Exception: " + e);
            }
        } else {
            Log.i("i","The previous request is remained. Please finished or cancel first");
        }
    }

    private SpassFingerprint.IdentifyListener mIdentifyListener = new SpassFingerprint.IdentifyListener() {
        @Override
        public void onFinished(int eventStatus) {
            Log.i("i","identify finished : reason =" + getEventStatusName(eventStatus));
            int FingerprintIndex = 0;
            String FingerprintGuideText = null;
            try {
                FingerprintIndex = mSpassFingerprint.getIdentifiedFingerprintIndex();
            } catch (IllegalStateException ise) {
                Log.e("e",ise.getMessage());
            }
            if (eventStatus == SpassFingerprint.STATUS_AUTHENTIFICATION_SUCCESS) {
                Log.i("i","onFinished() : Identify authentification Success with FingerprintIndex : " + FingerprintIndex);
                onDoingIdentify = false;
                if(!setting) {
                    editor.putInt("mainfinger", FingerprintIndex);
                    getFingerprintName(FingerprintIndex);
                    editor.putBoolean("setting", true);
                    editor.commit();
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage("등록하신 지문은 " + fingername + " 입니다 꼭 기억하세요 수정할 수 없습니다.").setCancelable(true)
                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            userdialog.dismiss();
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.setTitle("");
                    alertDialog.show();
                }
                else {
                    RequestParams params = new RequestParams();
                    params.put("uuid",uuid);
                    params.put("studentid",userid);
                    restClient.post("attendance", params, new TextHttpResponseHandler() {
                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, String responseString) {
                            onNomissIdentify = false;
                            if (responseString.equals("ok")) {
                                log("출석 성공");
                                fingerimg.setImageResource(R.drawable.complite);
                                AppStatus.getattnbtn().setClickable(false);
                                AppStatus.getattnbtn().setAlpha((float)0.5);
                                setditxt("출석에 성공하였습니다!");
                                new Handler() {
                                    @Override
                                    public void handleMessage(Message msg) {
                                        userdialog.dismiss();
                                    }
                                }.sendEmptyMessageDelayed(0,2000);
                            } else if(responseString.equals("late")) {
                                log("지각");
                                fingerimg.setImageResource(R.drawable.complite);
                                AppStatus.getattnbtn().setClickable(false);
                                AppStatus.getattnbtn().setAlpha((float)0.5);
                                setditxt("지각하였습니다.");
                                new Handler() {
                                    @Override
                                    public void handleMessage(Message msg) {
                                        userdialog.dismiss();
                                    }
                                }.sendEmptyMessageDelayed(0,2000);
                            } else if(responseString.equals("toolate")) {
                                log("결석");
                                fingerimg.setImageResource(R.drawable.complite);
                                AppStatus.getattnbtn().setClickable(false);
                                AppStatus.getattnbtn().setAlpha((float)0.5);
                                setditxt("결석하였습니다");
                                new Handler() {
                                    @Override
                                    public void handleMessage(Message msg) {
                                        userdialog.dismiss();
                                    }
                                }.sendEmptyMessageDelayed(0,2000);
                            } else if (responseString.equals("fail")) {
                                log("출석 실패");
                                fingerimg.setImageResource(R.drawable.complite);
                                setditxt("출석 실패하였습니다.");
                                new Handler() {
                                    @Override
                                    public void handleMessage(Message msg) {
                                        userdialog.dismiss();
                                    }
                                }.sendEmptyMessageDelayed(0,2000);
                            } else if (responseString.equals("notclass")){
                                log("올바른 강의실이 아닙니다");
                                fingerimg.setImageResource(R.drawable.complite);
                                setditxt("올바른 강의실이 아닙니다 강의실을 재 검색해주세요");
                                new Handler() {
                                    @Override
                                    public void handleMessage(Message msg) {
                                        userdialog.dismiss();
                                    }
                                }.sendEmptyMessageDelayed(0,2000);
                            } else if (responseString.equals("already")){
                                log("이미 출석 하였습니다");
                                fingerimg.setImageResource(R.drawable.complite);
                                setditxt("이미 출석한 강의입니다");
                                new Handler() {
                                    @Override
                                    public void handleMessage(Message msg) {
                                        userdialog.dismiss();
                                    }
                                }.sendEmptyMessageDelayed(0,2000);
                            } else if(responseString.equals("nottoday")){
                                log("출석 일자가 아닙니다");
                                fingerimg.setImageResource(R.drawable.complite);
                                setditxt("출석 일자가 아닙니다.");
                                new Handler() {
                                    @Override
                                    public void handleMessage(Message msg) {
                                        userdialog.dismiss();
                                    }
                                }.sendEmptyMessageDelayed(0,2000);
                            } else if(responseString.equals("notyet")){
                                log("출석 시간이 아닙니다");
                                fingerimg.setImageResource(R.drawable.complite);
                                setditxt("출석 가능한 시간이 아닙니다");
                                new Handler() {
                                    @Override
                                    public void handleMessage(Message msg) {
                                        userdialog.dismiss();
                                    }
                                }.sendEmptyMessageDelayed(0,2000);
                            } else if(responseString.equals("bad")){
                                log("데이터 삭제 혹은 재설치 하셨을 경우 당일엔 어플을 이용할 수 없습니다");
                                setditxt("데이터 삭제 혹은 재설치 하셨을 경우 당일엔 어플을 이용할 수 없습니다");
                                new Handler() {
                                    @Override
                                    public void handleMessage(Message msg) {
                                        userdialog.dismiss();
                                    }
                                }.sendEmptyMessageDelayed(0,2000);
                            }
                        }
                    });
                }
            } else if (eventStatus == SpassFingerprint.STATUS_AUTHENTIFICATION_PASSWORD_SUCCESS) {
                Log.i("i","onFinished() : Password authentification Success");
                setditxt("사용할리 없음");
            } else if (eventStatus == SpassFingerprint.STATUS_OPERATION_DENIED) {
                Log.i("i","onFinished() : Authentification is blocked because of fingerprint service internally.");
                setditxt("지문 인식 닫힘");
            } else if (eventStatus == SpassFingerprint.STATUS_USER_CANCELLED) {
                Log.i("i","onFinished() : User cancel this identify.");
                setditxt("사용자가 인식 종료함");
            } else if (eventStatus == SpassFingerprint.STATUS_TIMEOUT_FAILED) {
                Log.i("i","onFinished() : The time for identify is finished.");
                setditxt("시간이 초과하였습니다.");
            } else if (eventStatus == SpassFingerprint.STATUS_QUALITY_FAILED) {
                Log.i("i","onFinished() : Authentification Fail for identify.");
                onNomissIdentify = true;
                needRetryIdentify = true;
                setditxt("지문을 정확히 인식해주세요.\n 5회이상 오류시 잠시동안 인증이 불가능합니다");
                onNomissIdentify = true;
                FingerprintGuideText = mSpassFingerprint.getGuideForPoorQuality();
                Toast.makeText(mContext, FingerprintGuideText, Toast.LENGTH_SHORT).show();
            } else {
                Log.i("i","onFinished() : Authentification Fail for identify");
                onNomissIdentify = true;
                needRetryIdentify = true;
                setditxt("알맞은 지문을 인식해주세요.\n 5회이상 오류시 잠시동안 인증이 불가능합니다.");
            }
            if (!needRetryIdentify) {
                resetIdentifyIndex();
            }
        }

        @Override
        public void onReady() {
            Log.i("i","identify state is ready");
            if(setting) {
                if(!onNomissIdentify) {
                    setditxt("지문을 인식하세요");
                }
            } else {
                setditxt("인증에 사용하실 지문을 등록하세요 한번밖에 등록할 수 없습니다.");
            }
        }

        @Override
        public void onStarted() {
            Log.i("i","User touched fingerprint sensor");
        }

        @Override
        public void onCompleted() {
            Log.i("i","the identify is completed");
            onReadyIdentify = false;
            if (needRetryIdentify) {
                needRetryIdentify = false;
                mHandler.sendEmptyMessageDelayed(0,100);
            }
        }
    };


    private static String getEventStatusName(int eventStatus) {
        switch (eventStatus) {
            case SpassFingerprint.STATUS_AUTHENTIFICATION_SUCCESS:
                return "STATUS_AUTHENTIFICATION_SUCCESS";
            case SpassFingerprint.STATUS_AUTHENTIFICATION_PASSWORD_SUCCESS:
                return "STATUS_AUTHENTIFICATION_PASSWORD_SUCCESS";
            case SpassFingerprint.STATUS_TIMEOUT_FAILED:
                return "STATUS_TIMEOUT";
            case SpassFingerprint.STATUS_SENSOR_FAILED:
                return "STATUS_SENSOR_ERROR";
            case SpassFingerprint.STATUS_USER_CANCELLED:
                return "STATUS_USER_CANCELLED";
            case SpassFingerprint.STATUS_QUALITY_FAILED:
                return "STATUS_QUALITY_FAILED";
            case SpassFingerprint.STATUS_USER_CANCELLED_BY_TOUCH_OUTSIDE:
                return "STATUS_USER_CANCELLED_BY_TOUCH_OUTSIDE";
            case SpassFingerprint.STATUS_BUTTON_PRESSED:
                return "STATUS_BUTTON_PRESSED";
            case SpassFingerprint.STATUS_OPERATION_DENIED:
                return "STATUS_OPERATION_DENIED";
            case SpassFingerprint.STATUS_AUTHENTIFICATION_FAILED:
            default:
                return "STATUS_AUTHENTIFICATION_FAILED";
        }
    }

    private void setIdentifyIndex() {
        if (isFeatureEnabled_index) {
            if (mSpassFingerprint != null && designatedFingers != null) {
                mSpassFingerprint.setIntendedFingerprintIndex(designatedFingers);
            }
        }
    }

    private void resetIdentifyIndex() {designatedFingers = null;}

    private void cancelIdentify() {
        if (onReadyIdentify == true) {
            try {
                if (mSpassFingerprint != null) {
                    mSpassFingerprint.cancelIdentify();
                }
                Log.i("i","cancelIdentify is called");
            } catch (IllegalStateException ise) {
                Log.e("e",ise.getMessage());
            }
            onReadyIdentify = false;
            needRetryIdentify = false;
        } else {
            Log.i("i","Please request Identify first");
        }
    }

    private void getFingerprintName(int i) {
        SparseArray<String> mList = null;
        Log.i("i", "=Fingerprint Name=");
        if (mSpassFingerprint != null) {
            mList = mSpassFingerprint.getRegisteredFingerprintName();
        }
        if (mList == null) {
            Log.i("i", "Registered fingerprint is not existed.");
        } else {
            fingername = mList.get(i);
            Log.i("i", "index " + i + ", Name is " + fingername);
        }
    }

    private void toast (final String string){
        Toast.makeText(mContext, string,Toast.LENGTH_SHORT).show();
    }

    private void log(final String str) {
        ((MainActivity)mContext).runOnUiThread(new Runnable() {
            public void run() {
                AppStatus.settingstatustxt(str);
            }
        });
    }

    private void setditxt(final String str) {
        ((MainActivity)mContext).runOnUiThread(new Runnable() {
            public void run() {
                ditxt.setText(str);
            }
        });
    }
}
