package incheon.bfinga;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.TextHttpResponseHandler;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.util.Collection;
import java.util.Iterator;

import cz.msebera.android.httpclient.Header;
import incheon.bfinga.utils.RestClient;
import incheon.bfinga.utils.samsungfinger;

/**
 * Created by kyun on 2016-10-26.
 */
public class DoAttendance {
    private RestClient restClient;
    private Context mContext;
    private Button cssbtn;
    private LayoutInflater mInflater;
    private View v;

    private boolean isbeaconscan = false; //비콘 스캔중
    private boolean isdoble = false; //블루투스 온 오프 여부
    private boolean isfindclass = false; //강의실 검색 여부
    private Region region = new Region("justGiveMeEverything", null, null, null);
    private String beacontext, uuid;
    private ProgressDialog dialog;
    private samsungfinger samsungfinger;

    public DoAttendance (final Context context){
        this.mContext = context;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = mInflater.inflate(R.layout.layout_main,null);
        cssbtn = (Button) v.findViewById(R.id.classsearchbtn);
        AppStatus.setattnbtn((Button) v.findViewById(R.id.doattendancedtn));
        AppStatus.setstatustxt((TextView) v.findViewById(R.id.status));
        restClient = new RestClient(context);
        cssbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyBluetooth();
                if(isdoble) {
                    dialog = new ProgressDialog(mContext);
                    dialog.setTitle("");
                    dialog.setMessage("강의실 검색중...");
                    dialog.setIndeterminate(true);
                    dialog.setCancelable(true);
                    dialog.show();
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            if (isbeaconscan) {
                                stopscan();
                                Toast.makeText(mContext, "검색을 취소합니다", Toast.LENGTH_SHORT).show();
                                isbeaconscan = false;
                            } else {
                                classsearch();
                            }
                        }
                    });
                    doscan();
                }else {

                }
            }
        });
        AppStatus.getattnbtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isfindclass) {
                    samsungfinger = new samsungfinger(mContext, uuid);
                    samsungfinger.init();
                } else {
                    Toast.makeText(mContext, "강의실을 먼저 검색해 주세요", Toast.LENGTH_SHORT).show();
                }
            }
        });
        AppStatus.getattnbtn().setClickable(false);
        AppStatus.getattnbtn().setAlpha((float)0.5);
    }

    public void init(){
        AppStatus.settingview(v);
        AppStatus.settingtooltxt("BFINGA");
        verifyBluetooth();
    }

    private void verifyBluetooth() {
        try {
            if (!BeaconManager.getInstanceForApplication(mContext).checkAvailability()) {
                isdoble = false;
                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("블루투스가 켜져있지 않습니다");
                builder.setMessage("블루투스를 먼저 켜주세요");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
//                        finish();
//                        System.exit(0);
                    }
                });
                builder.show();
            }else {
                isdoble = true;
            }
        }
        catch (RuntimeException e) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle("BLE를 지원하지 않습니다");
            builder.setMessage("현재 이 기기는 BLE를 지원하지 않습니다 죄송합니다.");
            builder.setPositiveButton(android.R.string.ok, null);
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                @Override
                public void onDismiss(DialogInterface dialog) {
//                    finish();
//                    System.exit(0);
                }

            });
            builder.show();
        }
    }

    private void doscan(){
        isbeaconscan = true;
        AppStatus.getbm().setRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
                if (beacons.size() >0) { // 현재 한번만 탐지하고 중지
                    Iterator<Beacon> beaconIterator = beacons.iterator();
                    stopscan();
                    Beacon beacon = beaconIterator.next();
                    logGenericBeacon(beacon);
//                    while (beaconIterator.hasNext()) { //계속 비콘 탐지하는 코드
//
//                        // Debug - logging a beacon - checking background logging is working.
//                        System.out.println("Logging another beacon.");
//
//                    }
                }
            }
        });
        try {
            AppStatus.getbm().startRangingBeaconsInRegion(region);
        }catch (RemoteException e){

        }
    }

    private void stopscan(){
        try {
            AppStatus.getbm().stopRangingBeaconsInRegion(region);
            isbeaconscan = false;
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    } //스캔멈춤

    private void classsearch(){ //비콘 탐지 후 강의실 검색
        restClient.get("uuidsearch/" + uuid, null, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                if (responseString.equals("notclass")) {
                    log("올바르지 않은 강의입니다.");
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage("다시 검색하시겠습니까?").setCancelable(true)
                            .setPositiveButton("네", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    doscan();
                                }
                            }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.setTitle("올바르지 않은 강의입니다");
                    alertDialog.show();
                } else {
                    log(beacontext);
                    log(responseString + " 강의를 찾았습니다.");
                }
            }
        });
    }

    private void logGenericBeacon(Beacon beacon) {
        StringBuilder scanString = new StringBuilder();

        scanString.append(" UUID: ").append(beacon.getId1());

        scanString.append(" Maj. Mnr.: ");
        if (beacon.getId2() != null) {
            scanString.append(beacon.getId2());
        }
        scanString.append("-");
        if (beacon.getId3() != null) {
            scanString.append(beacon.getId3());
        }
        scanString.append(" RSSI: ").append(beacon.getRssi());

        beacontext = scanString.toString();
        uuid = beacon.getId1().toString();

        if(!(scanString.toString()).equals("")){
            isbeaconscan = false;
            isfindclass = true;
            AppStatus.getattnbtn().setClickable(true);
            AppStatus.getattnbtn().setAlpha((float)1);
            dialog.cancel();
        }
    }

    private void log(final String str) {
        ((MainActivity)mContext).runOnUiThread(new Runnable() {
            public void run() {
                AppStatus.settingstatustxt(str);
            }
        });
    }

}
