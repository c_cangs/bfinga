package incheon.bfinga.AttendanceLists;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import incheon.bfinga.AppStatus;
import incheon.bfinga.R;
import incheon.bfinga.utils.RestClient;

/**
 * Created by kyun on 2016-10-31.
 */
public class AttendanceList {
    private Context mContext;
    private RestClient restClient;
    private JSONArray attinfo = null;
    private LayoutInflater mInflater;
    private View v, innerv;
    private LinearLayout attlistview;


    public AttendanceList (Context context){
        this.mContext = context;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = mInflater.inflate(R.layout.layout_attlist,null);
        attlistview = (LinearLayout) v.findViewById(R.id.attlistview);

        restClient = new RestClient(context);
        restClient.get("getlist/" + AppStatus.getuserid(), null, new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                attinfo = response;
                Toast.makeText(mContext,"출석정보 로딩 성공",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void init(){
        if(attinfo == null){
            restClient.get("getlist/" + AppStatus.getuserid(), null, new JsonHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {

                }
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    attinfo = response;
                    Toast.makeText(mContext,"출석정보 로딩 성공",Toast.LENGTH_SHORT).show();
                    viewmaker();
                }
            });
        }
        else {
            viewmaker();
        }
    }

    private void viewmaker(){
        if(!attinfo.equals(null)) {
            attlistview.removeAllViews();
            AppStatus.settingview(v);
            AppStatus.settingtooltxt("출결 현황");
            TextView allattitemname;
            final LinearLayout allattitmes, allattitemview;
            View allitems;
            innerv = mInflater.inflate(R.layout.layout_attname, null);
            allattitemview = (LinearLayout) innerv.findViewById(R.id.attitmeview);
            allattitemview.setBackground(mContext.getDrawable(R.drawable.attname1));
            allattitemname = (TextView) innerv.findViewById(R.id.attitemname);
            allattitemname.setText("전체");
            allattitmes = (LinearLayout) innerv.findViewById(R.id.attitmes);
            allattitmes.setVisibility(View.GONE);
            allattitemname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int flag = allattitmes.getVisibility();
                    if(flag == View.GONE){
                        allattitemview.setBackground(mContext.getDrawable(R.drawable.attname2));
                        allattitmes.setVisibility(View.VISIBLE);
                    } else if(flag == View.VISIBLE){
                        allattitemview.setBackground(mContext.getDrawable(R.drawable.attname1));
                        allattitmes.setVisibility(View.GONE);
                    }
                }
            });
            allattitmes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int flag = allattitmes.getVisibility();
                    if(flag == View.GONE){
                        allattitemview.setBackground(mContext.getDrawable(R.drawable.attname2));
                        allattitmes.setVisibility(View.VISIBLE);
                    } else if(flag == View.VISIBLE){
                        allattitemview.setBackground(mContext.getDrawable(R.drawable.attname1));
                        allattitmes.setVisibility(View.GONE);
                    }
                }
            });
            for (int i = 0; i < attinfo.length() + 1; i++) {
                JSONObject object;
                TextView list_classname, list_attdatetime, list_attresult;
                allitems = mInflater.inflate(R.layout.layout_attitem,null);
                list_classname = (TextView) allitems.findViewById(R.id.list_classname);
                list_attdatetime = (TextView) allitems.findViewById(R.id.list_attdatetime);
                list_attresult = (TextView) allitems.findViewById(R.id.list_attresult);
                if(i == 0){
                    list_classname.setText("강의명");
                    list_attdatetime.setText("시간");
                    list_attresult.setText("결과");
                    allattitmes.addView(allitems);
                }  else {
                    try {
                        object = attinfo.getJSONObject(i - 1);
                        list_classname.setText(object.getString("classname"));
                        list_attdatetime.setText(object.getString("datetime"));
                        if (object.getInt("result") == 0) {
                            list_attresult.setText("결석");
                        } else if (object.getInt("result") == 1) {
                            list_attresult.setText("출석");
                        } else if (object.getInt("result") == 2) {
                            list_attresult.setText("지각");
                        }
                        allattitmes.addView(allitems);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            attlistview.addView(innerv);
            for(int k = 0; k < AppStatus.getclalist().length ; k++) {
                TextView attitemname;
                final LinearLayout attitmes,attitemview;
                View items;
                boolean first = false;
                innerv = mInflater.inflate(R.layout.layout_attname, null);
                attitemview = (LinearLayout) innerv.findViewById(R.id.attitmeview);
                attitemview.setBackground(mContext.getDrawable(R.drawable.attname1));
                attitemname = (TextView) innerv.findViewById(R.id.attitemname);
                attitmes = (LinearLayout) innerv.findViewById(R.id.attitmes);
                attitmes.setVisibility(View.GONE);
                attitemname.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int flag = attitmes.getVisibility();
                        if(flag == View.GONE){
                            attitmes.setVisibility(View.VISIBLE);
                            attitemview.setBackground(mContext.getDrawable(R.drawable.attname2));
                        } else if(flag == View.VISIBLE){
                            attitmes.setVisibility(View.GONE);
                            attitemview.setBackground(mContext.getDrawable(R.drawable.attname1));
                        }
                    }
                });
                attitmes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int flag = attitmes.getVisibility();
                        if(flag == View.GONE){
                            attitmes.setVisibility(View.VISIBLE);
                            attitemview.setBackground(mContext.getDrawable(R.drawable.attname2));
                        } else if(flag == View.VISIBLE){
                            attitmes.setVisibility(View.GONE);
                            attitemview.setBackground(mContext.getDrawable(R.drawable.attname1));
                        }
                    }
                });
                for (int i = 0; i < attinfo.length() + 1; i++) {
                    JSONObject object;
                    TextView list_classname, list_attdatetime, list_attresult;
                    items = mInflater.inflate(R.layout.layout_attitem,null);
                    list_classname = (TextView) items.findViewById(R.id.list_classname);
                    list_attdatetime = (TextView) items.findViewById(R.id.list_attdatetime);
                    list_attresult = (TextView) items.findViewById(R.id.list_attresult);
                    if(i == 0){
                        list_classname.setText("강의명");
                        list_attdatetime.setText("시간");
                        list_attresult.setText("결과");
                        attitmes.addView(items);
                    }  else {
                        try {
                            object = attinfo.getJSONObject(i - 1);
                            if (AppStatus.getclalist()[k].equals(object.getString("classid"))) {
                                if (!first) {
                                    attitemname.setText(object.getString("classname"));
                                    first = true;
                                }
                                list_classname.setText(object.getString("classname"));
                                list_attdatetime.setText(object.getString("datetime"));
                                if (object.getInt("result") == 0) {
                                    list_attresult.setText("결석");
                                } else if (object.getInt("result") == 1) {
                                    list_attresult.setText("출석");
                                } else if (object.getInt("result") == 2) {
                                    list_attresult.setText("지각");
                                }
                                attitmes.addView(items);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                attlistview.addView(innerv);
            }
        }
    }
}
