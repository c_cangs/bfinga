package incheon.bfinga.AttendanceLists;

/**
 * Created by kyun on 2016-11-01.
 */
public class Att_items {
    private String classname;
    private int position;

    public String getCname(){
        return classname;
    }

    public int getposition() {
        return position;
    }

    public Att_items (String s, int i){
        this.classname = s;
        this.position = i;
    }
}
